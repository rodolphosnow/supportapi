class BarcodeServices

    def self.calculate_check_digit(barcode)
        begin
            #convert barcode to string
            barcode = barcode.to_s
            return { status: 422, check_digit: nil, message: "Error! Invalid barcode, barcode must have only numbers"} unless !!/\A[+-]?\d+(\.\d+)?\z/.match(barcode)
            return { status: 422, check_digit: nil, message: "Error! The barcode must have 12 digits." } if barcode.length != 12
            multiplier_list = [1,3]
            total = 0
            position = 0
            # Take each digit, from left to right and multiply them alternatively by 1 and 3 while adding to make the sum.
            barcode.split(//).each do |number|
            total += number.to_i * multiplier_list[position]
            position ^= 1
            end
            # Mod 10 of the sum
            total = 10 - (total % 10)
            total = 0 if total == 10 
            { status: 200, check_digit: total, message: "success" }
        rescue
            { status: 422, check_digit: nil, message: "something went wrong, please contact the system administrator" }
        end
    end
end
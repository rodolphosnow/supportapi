class Api::V1::BarcodeController < ApplicationController
    
    def calculate_check_digit
        barcode = params[:barcode]
        response = BarcodeServices.calculate_check_digit(barcode)
        render json: response, status: response[:status]
    end

end

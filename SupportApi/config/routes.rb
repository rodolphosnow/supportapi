Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  namespace :api, defaults: { format: :json }, path: 'api' do
    namespace :v1 , path: 'v1' do
      resources :barcode, path: 'barcode', only: [] do
        post 'calculate-check-digit', action: 'calculate_check_digit', on: :collection
      end
    end
  end
  
end

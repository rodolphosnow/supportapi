require 'rails_helper'

RSpec.describe "Api::V1::Barcodes", type: :request do
    describe 'Get/barcode/calculate-check-digit/:barcode' do
        before do
			post "/api/v1/barcode/calculate-check-digit", params: {barcode: barcode_params}
        end
        
        context 'when the params are valid' do
			let(:barcode_params)  { "978014300723" } 
            let(:expected_digit) { 4 }
			it 'returns status code 200' do
				expect(response).to have_http_status(200)
			end

            it 'returns the json for the check digit' do
                json_body = JSON.parse(response.body, symbolize_names: true)
				expect(json_body[:check_digit]).to eq(expected_digit)
			end

		end

		context 'when te params are invalid' do
			let(:barcode_params){ "11" }
			it 'returns status code 422' do
				expect(response).to have_http_status(422)
			end

            it 'returns the json error for the barcode' do
                json_body = JSON.parse(response.body, symbolize_names: true)
				expect(json_body[:message]).to eq("Error! The barcode must have 12 digits.")
			end
		end
    end
end

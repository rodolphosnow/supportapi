require 'rails_helper'

RSpec.describe BarcodeServices do
    describe 'calculate_check_digit' do
        context 'with wrong params' do 
            it 'should return error message when barcode length != 12' do
                response = subject.class.calculate_check_digit("11111111")
                response[:message].should == "Error! The barcode must have 12 digits."
                response[:check_digit].should == nil
                response[:status].should == 422
            end

            it 'should return error message when barcode does not have only numbers' do
                response = subject.class.calculate_check_digit("12345678910a")
                response[:message].should == "Error! Invalid barcode, barcode must have only numbers"
                response[:check_digit].should == nil
                response[:status].should == 422
            end
        end

        context 'with valid barcode' do
            it 'should return error message when barcode length != 12' do
                response = subject.class.calculate_check_digit("978014300723")
                response[:check_digit].should == 4
                response[:message].should == "success"
                response[:status].should == 200
            end
        end
        
    end
end